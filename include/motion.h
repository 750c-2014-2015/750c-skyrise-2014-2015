#ifndef MOTION_H

#define MOTION_H

#include <API.h>
#include <math.h>

/***********************************Motor Definitions*********************************/
#define RIGHT_FRONT_MOTOR 9
#define LEFT_FRONT_MOTOR 2
#define RIGHT_BACK_MOTOR 3
#define LEFT_BACK_MOTOR 8
//#define RIGHT_MID_MOTOR 4
//#define LEFT_MID_MOTOR 5
#define ARM_BOTTOM_LEFT 10 //flipped
#define ARM_BOTTOM_RIGHT 6 //not flipped
#define ARM_TOP_LEFT 4 //not flipped
#define ARM_TOP_RIGHT 5 //not flipped
#define ARM_MID_LEFT 1 //not flipped
#define ARM_MID_RIGHT 7 //not flipped
/*************************************************************************************/

/***********************************Limit Switch Definitions*********************************/
#define ARM_BOTTOM_LIMIT 2
#define ARM_TOP_LIMIT 3
#define ARM_UP_LIMIT 12 //the limit switch that is pushed when the arm is going upwards
#define ARM_DOWN_LIMIT 5
/********************************************************************************************/

/***********************************Autonomous Selection Definitions*********************************/
#define AUTON_POT 1
#define AUTON_BUTTON 8
/****************************************************************************************************/

/***********************************Autonomous Recorder Definitions*********************************/
#define JOY_POLL_FREQ 50
#define AUTON_TIME 15
#define PROGSKILL_TIME 60
#define MAX_AUTON_SLOTS 10
#define AUTON_FILENAME_MAX_LENGTH 8
/***************************************************************************************************/

/***********************************Movement Definitions*********************************/
#define MOTOR_THRESHOLD 25
#define TOTAL_ARM_TICKS 4
#define ARM_VALUE_SHIFT 2
/****************************************************************************************/

extern int armHeight; //the number of limit switch clicks that the arm has reached
extern int armUpLimitClicked;
extern int armDownLimitClicked;

typedef struct joyState {
    //unsigned char joyPneumaticOpen, joyPneumaticClosed;
    signed char joyStrafeX, joyStrafeY, joyRotation, joyArmValue;
} joyState;

extern joyState states[AUTON_TIME*JOY_POLL_FREQ];

/** 
 * Constrains motor values to a defined minimum value.
 * 
 * @param value the value to constrain
 * 
 * @return 0 if value is lower than MOTOR_THRESHOLD, value otherwise.
 */
inline int threshold(int value) {
    if (abs(value) < MOTOR_THRESHOLD) {
        return 0;
    }
    return value;
}

/** 
 * Moves the drive motors.
 * 
 * @param strafeX the horizontal movement component
 * @param strafeY the vertical movement component
 * @param rotation the rotational movement component
 */
inline void move(int strafeX, int strafeY, int rotation) {
    // middle motor is geared at a ratio of 84:60, 1.4:1
    motorSet(RIGHT_FRONT_MOTOR, threshold(rotation + strafeX - strafeY));
    motorSet(LEFT_FRONT_MOTOR, threshold(rotation + strafeX + strafeY));
    motorSet(RIGHT_BACK_MOTOR, threshold(rotation - strafeX - strafeY));
    motorSet(LEFT_BACK_MOTOR, threshold(rotation - strafeX + strafeY));
    //motorSet(RIGHT_MID_MOTOR, threshold(-rotation / 1.4 + strafeY));
    //motorSet(LEFT_MID_MOTOR, threshold(-rotation / 1.4 - strafeY));
}

/** 
 * Lifts the elevator and records the current arm height (in ticks).
 * 
 * @param inSpeed the desired motor speed for the arm motors
 */
void elevator(int inSpeed);

/** 
 * Lifts the elevator to the target height (in ticks).
 * 
 * @param tickHeight the target height (in ticks)
 * @param speed the speed at which to run the arm motors
 * 
 * @return 0 if not at target height yet, 1 if at target height
 */
int elevatorPos(int ticks, int speed);

//inline void solenoid(int state) {
//   digitalWrite(SOLENOID_PIN, state);
//}

/** 
 * Initializes autonomous recorder by setting states array to zero.
 */
void initAutonRecorder();

/** 
 * Records driver joystick values into states array.
 */
void recordAuton();

/** 
 * Replays autonomous based on loaded values in states array.
 */
void playbackAuton();

/** 
 * Saves contents of the states array to a file.
 */
void saveAuton();

/** 
 * Loads autonomous file contents into states array.
 */
void loadAuton();

void recordJoyInfo();

// moves the robot based on the joystick values gathered
void moveRobot();

#endif
