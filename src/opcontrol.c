/** @file opcontrol.c
 * @brief File for operator control code
 *
 * This file should contain the user operatorControl() function and any functions related to it.
 *
 * Copyright (c) 2011-2014, Purdue University ACM SIG BOTS.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Purdue University ACM SIG BOTS nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL PURDUE UNIVERSITY ACM SIG BOTS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Purdue Robotics OS contains FreeRTOS (http://www.freertos.org) whose source code may be
 * obtained from http://sourceforge.net/projects/freertos/files/ or on request.
 */

#include "main.h"
#include "motion.h"
#include <math.h>

//int joyPneumaticOpen;
//int joyPneumaticClosed;
int joyArmValue;
int joyStrafeX;
int joyStrafeY;
int joyRotation;

int armHeight = 0; 
int armUpLimitClicked = HIGH;
int armDownLimitClicked = HIGH;

int joyEightLeftPressed = 0;
int joyEightRightPressed = 0;

/** 
 * Lifts the elevator and records the current arm height (in ticks).
 * 
 * @param inSpeed the desired motor speed for the arm motors
 */
void elevator(int inSpeed) {
    int speed = inSpeed;
    if (armHeight >= TOTAL_ARM_TICKS) {
        speed = 0.7 * inSpeed;
    }
    if(((speed < 0) && digitalRead(ARM_BOTTOM_LIMIT) == HIGH) || ((speed >= 0) && digitalRead(ARM_TOP_LIMIT) == HIGH)) {
        if (speed > 0) {
            if (armUpLimitClicked == LOW && digitalRead(ARM_UP_LIMIT) == HIGH) {
                armHeight++;
            }
            armUpLimitClicked = digitalRead(ARM_UP_LIMIT);
        } else {
            if (armDownLimitClicked == LOW && digitalRead(ARM_DOWN_LIMIT) == HIGH) {
                armHeight--;
            }
            armDownLimitClicked = digitalRead(ARM_DOWN_LIMIT);
        }

        motorSet(ARM_BOTTOM_LEFT, -speed);
        motorSet(ARM_BOTTOM_RIGHT, speed);
        motorSet(ARM_MID_LEFT, speed);
        motorSet(ARM_MID_RIGHT, -speed);
        motorSet(ARM_TOP_LEFT, speed);
        motorSet(ARM_TOP_RIGHT, -speed);
    } else {
        if (digitalRead(ARM_BOTTOM_LIMIT) == LOW) {
            armHeight = 0;
        } else if (digitalRead(ARM_TOP_LIMIT) == LOW) {
            armHeight = 1 + TOTAL_ARM_TICKS;
        }
        motorSet(ARM_BOTTOM_LEFT, 0);
        motorSet(ARM_BOTTOM_RIGHT, 0);
        motorSet(ARM_TOP_LEFT, 0);
        motorSet(ARM_TOP_RIGHT, 0);
        motorSet(ARM_MID_LEFT, 0);
        motorSet(ARM_MID_RIGHT, 0);
    }
}

/** 
 * Lifts the elevator to the target height (in ticks).
 * 
 * @param tickHeight the target height (in ticks)
 * @param speed the speed at which to run the arm motors
 * 
 * @return 0 if not at target height yet, 1 if at target height
 */
int elevatorPos(int tickHeight, int speed) {
    if (armHeight != tickHeight) {
        elevator(abs(speed) * (tickHeight - armHeight) / abs(tickHeight - armHeight));
    } else {
        elevator(0);
        armHeight = tickHeight;
        return 1;
    }
    return 0;
}

/** 
 * Moves the robot (elevator and drivetrain) based on joystick values.
 */
void moveRobot() {
    if (joyArmValue <= 1) {
        elevator(joyArmValue *  127);
    } else if (joyArmValue == 0 + ARM_VALUE_SHIFT) {
        if (digitalRead(ARM_BOTTOM_LIMIT) == LOW) {
            elevator(0);
            armHeight = 0;
            joyArmValue = 0;
        } else {
            elevator(-127);
        }
    } else if (joyArmValue == TOTAL_ARM_TICKS + 1 + ARM_VALUE_SHIFT) {
        if (digitalRead(ARM_TOP_LIMIT) == LOW) {
            elevator(0);
            armHeight = TOTAL_ARM_TICKS + 1;
            joyArmValue = 0;
        } else {
            elevator(127);
        }
    } else if (joyArmValue > ARM_VALUE_SHIFT && joyArmValue <= TOTAL_ARM_TICKS + ARM_VALUE_SHIFT) {
        if (elevatorPos(joyArmValue - ARM_VALUE_SHIFT, 127)) {
            joyArmValue = 0;
        }
    }

    move(joyStrafeX, joyStrafeY, joyRotation);
}

/** 
 * Reads from the joystick to determine movement parameters.
 */
void recordJoyInfo() {
    //joyPneumaticOpen = joystickGetDigital(1,5,JOY_UP);
    //joyPneumaticClosed = joystickGetDigital(1,5,JOY_DOWN);

    if (joystickGetDigital(1, 6, JOY_UP) || joystickGetDigital(2, 6, JOY_UP)) {
        joyArmValue = 1;
    } else if (joystickGetDigital(1, 6, JOY_DOWN) || joystickGetDigital(2, 6, JOY_DOWN)) {
        joyArmValue = -1;
    } else if (!joystickGetDigital(2, 8, JOY_LEFT) && joyEightLeftPressed) {
        if (joyArmValue <= 1) {
            joyArmValue = ARM_VALUE_SHIFT + ((armHeight <= TOTAL_ARM_TICKS) ? armHeight + 1 : TOTAL_ARM_TICKS + 1);
        } else {
            joyArmValue = ((joyArmValue - ARM_VALUE_SHIFT) <= TOTAL_ARM_TICKS) ? (joyArmValue + 1) : (ARM_VALUE_SHIFT + TOTAL_ARM_TICKS + 1);
        }
    } else if (!joystickGetDigital(2, 8, JOY_RIGHT) && joyEightRightPressed) {
        if (joyArmValue <= 1) {
            joyArmValue = ARM_VALUE_SHIFT + ((armHeight > 0) ? (armHeight - 1) : 0);
        } else {
            joyArmValue = ((joyArmValue - ARM_VALUE_SHIFT) > 0) ? (joyArmValue - 1) : (ARM_VALUE_SHIFT);
        }
    } else if (joystickGetDigital(2, 8, JOY_UP)) {
        joyArmValue = ARM_VALUE_SHIFT + TOTAL_ARM_TICKS + 1;
    } else if (joystickGetDigital(2, 8, JOY_DOWN)) {
        joyArmValue = ARM_VALUE_SHIFT;
    } else if (joyArmValue <= 1) {
        joyArmValue = 0;
    }

    joyStrafeX = joystickGetAnalog(1, 4);
    joyStrafeY = joystickGetAnalog(1, 3);
    joyRotation = joystickGetAnalog(1, 1);

    if(joystickGetDigital(1, 7, JOY_LEFT)){
        joyStrafeX = -127;
    } else if(joystickGetDigital(1, 7, JOY_RIGHT)){
        joyStrafeX = 127;
    }

    if (joystickGetDigital(2, 8, JOY_LEFT)) {
        joyEightLeftPressed = HIGH;
    } else {
        joyEightLeftPressed = LOW;
    }

    if (joystickGetDigital(2, 8, JOY_RIGHT)) {
        joyEightRightPressed = HIGH;
    } else {
        joyEightRightPressed = LOW;
    }
}

/** 
 * Main function that is run during operator control period.
 */
void operatorControl() {
    lcdSetBacklight(LCD_PORT, false);
    while (true) {
        if(isOnline() || progSkills == 0){
            lcdSetText(LCD_PORT, 1, "Op control...");
            lcdSetText(LCD_PORT, 2, "");
            recordJoyInfo();
            if(digitalRead(ARM_UP_LIMIT) == LOW || digitalRead(ARM_DOWN_LIMIT) == LOW) {
                lcdSetBacklight(LCD_PORT, true);
            } else if(digitalRead(ARM_UP_LIMIT) == HIGH && digitalRead(ARM_DOWN_LIMIT) == HIGH) {
                lcdSetBacklight(LCD_PORT, false);
            }
            if (joystickGetDigital(1, 8, JOY_RIGHT) && !isOnline()) {
                recordAuton();
                saveAuton();
            } else if (joystickGetDigital(1, 8, JOY_LEFT) && !isOnline()) {
                loadAuton();
                playbackAuton();
            }
            moveRobot();
        } else {
            motorStopAll();
            lcdSetText(LCD_PORT, 1, "Press 8R");
            lcdPrint(LCD_PORT, 2,   "Last Skills: %d", progSkills);
            if (joystickGetDigital(1, 8, JOY_RIGHT) && !isOnline()) {
                recordAuton();
                saveAuton();
            } else if(joystickGetDigital(1, 8 , JOY_UP) && !isOnline()) {
                progSkills = 0;
            }
        }
        delay(20);
    }
}
